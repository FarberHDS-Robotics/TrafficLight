// Global Variables Used in Code
const int red = 8;
const int yellow = 9;
const int green = 10;
const int ledDelay = 5000;

void setup() {
  for (int i = 8; i < 10; i++)
  {
    // Initialize all the pins
    pinMode(i, OUTPUT);
  }
}

void loop() {
  // Red Light
  digitalWrite(red, HIGH);
  delay(ledDelay * 2);
  digitalWrite(red, LOW);

  // Green Light
  digitalWrite(green, HIGH);
  delay(ledDelay);
  digitalWrite(green, LOW);

  // Yellow Light
  digitalWrite(yellow, HIGH);
  delay(ledDelay / 2);
  digitalWrite(yellow, LOW);
}
